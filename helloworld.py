# print("Hello World")

# message = 'Hello'
# print(message)
# message = message + "World"
# print(message)

# a = 1.5
# b = 0.5
# print(a+b)
# print(a*b)
# print(a/b)

# a = 2
# b = 3
# print(a**b)
# print(b%a)

# print(1 == 2)
# a = 1
# b = 2
# print(a == 1)
# print( a != b)

# print(bool(-1))
# print(bool(1))
# print(bool(2))
# print(bool(0))
# print(bool(0.0))
# print(bool(""))
# print(bool("coś"))
# print(bool(" "))
# print(bool(None))

a = "Ciąg znaków"
print(type(a))
print(a is str)
print(a is not int)
print("1"+"2")
print(int("1")+int("2"))
print(bool(1))
print(str(12))
